from daoOracle import daoOracle
from daoCSV import daoCSV
from _dto_Tankem import TankemBalanceDTO 
import Tkinter
import tkMessageBox


class modifierBalance():

    def modifier(self):
        dCSV = daoCSV()
        dOracle = daoOracle() 
        if dOracle.connection():
            dto= dCSV.read()
            dOracle.update(dto)
        else:
            root=Tkinter.Tk()
            root.withdraw()
            tkMessageBox.showinfo("ERREUR", "Impossible de se connecter a la BD")
    
l = modifierBalance()
l.modifier()

