from daoOracle import daoOracle
from daoCSV import daoCSV
from _dto_Tankem import TankemBalanceDTO 
import Tkinter
import tkMessageBox


class lireBalance():
    def lire(self):
        dCSV = daoCSV()
        dOracle = daoOracle()
        if dOracle.connection() == True:
            dto=dOracle.read()        
            dCSV.create(dto)
        else:
            root=Tkinter.Tk()
            root.withdraw()
            tkMessageBox.showinfo("ERREUR", "Impossible de se connecter a la BD")
l = lireBalance()
l.lire()
