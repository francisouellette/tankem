
CREATE TABLE Favoris(
	nomMap VARCHAR2(100) NOT NULL,
	nomJoueur VARCHAR2(100) NOT NULL,
	CONSTRAINT pk_MapJoueurFavoris PRIMARY KEY (nomMap,nomJoueur)
);

CREATE TABLE NbFoisJoue(
	nomMap VARCHAR2(100) NOT NULL,
	nomJoueur VARCHAR2(100) NOT NULL,
	nbJoue NUMBER,
	CONSTRAINT pk_MapJoueurNbJoue PRIMARY KEY (nomMap,nomJoueur)
);

