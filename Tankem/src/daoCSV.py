from _dto_Tankem import TankemBalanceDTO 
import csv
from Tkinter import *
import tkMessageBox
import Tkinter as tk
import tkFileDialog
import os

class daoCSV():
    def read(self): 
        root=tk.Tk()
        root.withdraw()
        bureau=os.path.expanduser('~/desktop')
        destination = tkFileDialog.askopenfilename(initialdir=bureau,filetypes = [("CSV","*.csv")])
        csv_in = open(destination,'rb')
        
        myreader = csv.reader(csv_in)
        datas, valeurs = zip(*myreader)
        dto = TankemBalanceDTO()
        for i in range(len(datas)):
            dto.setInfo(datas[i],valeurs[i])

        return dto
            
    
    def create(self,dto):
        root=tk.Tk()
        root.withdraw()
        bureau=os.path.expanduser('~/desktop')
        destination = tkFileDialog.askdirectory(initialdir=bureau)

        if destination is None: 
            return 
        print(destination)
        
        datas =[]
        valeurs = []
        
        for data in dto.getDic():
            value = dto.getInfo(data)
            datas.append(data)
            valeurs.append(value)
        
        csv_out = open(destination+"/valeurBalance.csv", "wb") 

        mywriter =csv.writer(csv_out)    
        row = zip(datas,valeurs)
        mywriter.writerows(row)





