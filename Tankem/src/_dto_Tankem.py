

class TankemBalanceDTO():

    def __init__(self):
	    self.dictBalance={} 

    def  getInfo(self,nomVar) :
            return self.dictBalance[nomVar]
        
    def getDic(self):
        return self.dictBalance
    
    def setInfo(self,nomVar,valeur) :
        self.dictBalance.update({nomVar:valeur})
        
    def __getitem__(self, key):
        return self.dictBalance[key]
        
dictonaire =  TankemBalanceDTO()

