
from Tkinter import *
import tkMessageBox
import Tkinter as tk
#import Image, ImageTk
from math import floor
from mapEditorDTO import MapEditorDTO
from daoOracleMap import daoOracleMap
from editorMain import run

import os


class runs():
    
    def __init__(self):#?
        self.daoMap = daoOracleMap()
        dtoNomMap = MapEditorDTO()
        
        self.root = Tk()
        self.root.bind('<Left>', self.leftClick)
        self.root.bind('<Right>',self.rightClick)
        
        dtoNomMap=self.daoMap.readNomMap()
        self.nameLevelList = dtoNomMap.getInfo("Nom Level")
        self.dtoValeursMap = self.daoMap.readMap(self.nameLevelList[0])
        self.valeurX = self.dtoValeursMap.getInfo("Size X")
        self.valeurY = self.dtoValeursMap.getInfo("Size Y") 
        self.tempsMin = self.dtoValeursMap.getInfo("Temps Min")
        self.tempsMax = self.dtoValeursMap.getInfo("Temps Max")
        self.nomCreateur = self.dtoValeursMap.getInfo("Users")
        self.nbFoisjouer =0#GET Nb Games 
         
        self.levelSelected = 0
        self.dtoValeursMap = MapEditorDTO()

        self.setUpMenu()

        self.root.mainloop()
        try:
            self.root.withdraw()
        except:
            pass
    
    def setUpMenu(self):
        ###CREATION DE NOS TROIS FRAMES 
        self.frameMain= Frame(self.root,width=1250,height=500)
        self.frameMain.pack()
        self.frameChoix= Frame(self.frameMain,width=350,height=500)
        self.frameChoix.grid_propagate(False)
        self.canvas = Canvas(self.frameMain,width=500,height=500,)
        self.canvas.grid_propagate(False)
        self.frameInfo= Frame(self.frameMain,width=200,height=500)
        self.frameInfo.grid_propagate(False)
        
        
        #######################
        #Frame Choix Level 
        #######################
        self.stringNomMap = StringVar() 
        self.stringNomMap.set(self.nameLevelList[self.levelSelected])
        self.MapTitle = Label(self.frameChoix,textvariable=self.stringNomMap,font=("Helvetica", 15))
        self.MapTitle.grid(row=0,pady=75)
        self.FrameButton = Frame(self.frameChoix, width=350, height = 75)
        self.FrameButton.grid(row=1,padx=50)
        
        createNewMap = Button(self.frameChoix,text="Creer MAP",font=("Helvetica", 15),command = self.createNewMap)
        createNewMap.grid(row=3, pady=25)
        btnOk = Button(self.frameChoix,text="OK!",font=("Helvetica", 15),command = self.close)
        btnOk.grid(row=2, pady=40)
        
        btnRandom = Button(self.frameChoix,text="RANDOM!!!",font=("Helvetica", 15),command = self.randomMap)
        btnRandom.grid(row = 4)

     
        arrowLeft = Button(self.FrameButton,text="Previous",width = 10,command = lambda:self.changeLevel(-1))
        arrowRight = Button(self.FrameButton,text="Next",width = 10,command = lambda:self.changeLevel(1))
        
        arrowLeft.grid(row=1,column=1,padx=15)
        arrowRight.grid(row=1,column=3,padx=30)
        
        #######################
        #Frame Info Level 
        #######################

        Label(self.frameInfo,text="Grosseur X = ").grid(row=0,column=1,padx=15)
        self.stringValeurX = StringVar() 
        self.stringValeurX.set(self.valeurX)
        grosseurX = Label(self.frameInfo,textvariable=self.stringValeurX)
        grosseurX.grid(row=0,column=2,padx=15,pady=15)
        
        Label(self.frameInfo,text="Grosseur Y = ").grid(row=1,column=1,padx=15)
        self.stringValeurY = StringVar() 
        self.stringValeurY.set(self.valeurY)
        grosseurY = Label(self.frameInfo,textvariable=self.stringValeurY)
        grosseurY.grid(row=1,column=2,padx=15,pady=10)
        
          
        Label(self.frameInfo,text="Temps Min = ").grid(row=2,column=1,padx=15)
        self.stringTempsMin = StringVar() 
        self.stringTempsMin.set(self.tempsMin)
        tempsMin = Label(self.frameInfo,textvariable=self.stringTempsMin)
        tempsMin.grid(row=2,column=2,padx=15,pady=10)
    
    
        Label(self.frameInfo,text="Temps Max = ").grid(row=3,column=1,padx=15)
        self.stringTempsMax = StringVar() 
        self.stringTempsMax.set(self.tempsMax)
        tempsMax = Label(self.frameInfo,textvariable=self.stringTempsMax)
        tempsMax.grid(row=3,column=2,padx=15,pady=10)
        
            
        Label(self.frameInfo,text="Createur Niveau : ").grid(row=4,column=1,padx=15)
        self.stringNomCreateur = StringVar() 
        self.stringNomCreateur.set(self.nomCreateur)
        nomCreateur = Label(self.frameInfo,textvariable=self.stringNomCreateur)
        nomCreateur.grid(row=4,column=2,padx=15,pady=10)
        
        Label(self.frameInfo,text="Nb fois jouer = ").grid(row=5,column=1,padx=15)
        self.stringNbGame = StringVar() 
        self.stringNbGame.set(self.nbFoisjouer)
        nbGame = Label(self.frameInfo,textvariable=self.stringNbGame)
        nbGame.grid(row=5,column=2,padx=15,pady=10)
        self.initMatrice()

        self.frameChoix.pack(side=LEFT)
        self.canvas.pack(side=LEFT)
        self.frameInfo.pack(side=LEFT,pady=60,padx=40)
        
    def close(self):
        self.dtoValeursMap = self.daoMap.readMap(self.stringNomMap.get())
        self.root.destroy()
        
    def getDtoMap(self):
        return self.dtoValeursMap
        
    def leftClick(self,event):
        self.changeLevel(-1)
        
    def rightClick(self,event):
        self.changeLevel(1)

    def changeLevel(self,valeur):
        self.levelSelected += valeur
        if self.levelSelected < 0: 
            self.levelSelected = 0
        elif self.levelSelected >= len(self.nameLevelList ):
            self.levelSelected = len(self.nameLevelList)-1
        self.stringNomMap.set(self.nameLevelList[self.levelSelected])
        self.dtoValeursMap = self.daoMap.readMap(self.stringNomMap.get())
        self.valeurX = self.dtoValeursMap.getInfo("Size X")
        self.stringValeurX.set( self.valeurX)
        self.valeurY= self.dtoValeursMap.getInfo("Size Y")
        self.stringValeurY.set(self.valeurY)
        self.tempsMin = self.dtoValeursMap.getInfo("Temps Min")
        self.stringTempsMin.set(self.tempsMin)
        self.tempsMax = self.dtoValeursMap.getInfo("Temps Max")
        self.stringTempsMax.set(self.tempsMax)
        self.nomCreateur= self.dtoValeursMap.getInfo("Users")
        self.stringNomCreateur.set(self.nomCreateur)

        
        self.initMatrice()
    
    def createNewMap(self):
        self.root.destroy()
        mapCreator = run()
        self.__init__()
        
    def randomMap(self):
        self.dtoValeursMap = "RandomMap"
        self.root.destroy()
    
    def initMatrice(self):
            self.canvas.delete('all')
            self.dtoValeursMap = self.daoMap.readMap(self.stringNomMap.get())
            self.matrice= [[0] * self.valeurY for i in range(self.valeurX)]    
            sizeXcanvas = 400
            sizeYcanvas = 400
            compteur = 0
            
            for i in range(self.valeurY):
                for j in range(self.valeurX):
                    initx = floor(sizeXcanvas / self.valeurX * j ) # <-- Code trouver online pour afficher matrice dans un espace predeterminer 
                    inity = floor(sizeYcanvas / self.valeurY * i) # 
                    endx = floor(sizeXcanvas / self.valeurX * (j+1)) # 
                    endy = floor(sizeYcanvas / self.valeurY * (i+1)) # 
                    valeur=self.dtoValeursMap.getInfo("Matrice Jeu")[j][i]
                  
                    if valeur == "plancher":
                        self.matrice[j][i] = self.canvas.create_rectangle(initx, inity, endx, endy, fill="#6F532A")
                    elif valeur=="mur":
                        self.matrice[j][i] = self.canvas.create_rectangle(initx, inity, endx, endy, fill="purple")
                    elif valeur=="up":
                        self.matrice[j][i] = self.canvas.create_rectangle(initx, inity, endx, endy, fill="green")
                    else:
                        self.matrice[j][i] = self.canvas.create_rectangle(initx, inity, endx, endy, fill="red")
                
                    compteur +=1
#runs()