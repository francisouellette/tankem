from Tkinter import *
import tkMessageBox
import Tkinter as tk
from DTOJoueur import DTOJoueur
from DAOOracleUser import DAOOracleUser
import bcrypt



class OublieMdp():
    def __init__(self,nomJoueur):
           self.root = Tk()
           self.root.title("Oublie/Changer Mot de passe")
           self.nomJoueur = tk.StringVar(self.root)
           self.nomJoueur.set(nomJoueur)
           self.setUpMenu(nomJoueur)
           self.root.mainloop()
           

      
    def setUpMenu(self, nomJoueur):
        #Get les questions de notre BD selon nom joueur (hardcoder pour l'instant!
        self.dao = DAOOracleUser()
        self.dto = DTOJoueur()
        self.dto = self.dao.readUserMDP(nomJoueur)
        self.questionA = self.dto.getInfo('QuestionA') + '?'
        self.questionB = self.dto.getInfo('QuestionB') + '?'
        self.frameMain= Frame(self.root,width=450,height = 575)
        self.frameMain.pack_propagate(0)
        
        #Creation des stringvar ou les donnes vont etre rempli
        self.reponseQuestionA = tk.StringVar(self.root)
        self.reponseQuestionB = tk.StringVar(self.root)
        self.password1 = tk.StringVar(self.root)
        self.password2 = tk.StringVar(self.root)
        self.reponse=tk.StringVar(self.root)
        
        #Fonction qui active le bouton de confirmation quand tous les champs sont remplis
        def entryRempli(*args):
            entryA=self.nomJoueur.get()
            entryB=self.reponseQuestionA.get()
            entryC=self.reponseQuestionB.get()
            entryD=self.password1.get()
            entryE=self.password2.get()

            if entryA and entryB and entryC and entryD and entryE:
                self.confirm.config(state='normal')
            else:
                self.confirm.config(state='disabled')

     
        #affecte la fonction entryRempli a mes strinvar
        self.nomJoueur.trace("w", entryRempli)
        self.reponseQuestionA.trace("w",entryRempli)
        self.reponseQuestionB.trace("w", entryRempli)
        self.password1.trace("w", entryRempli)
        self.password2.trace("w", entryRempli)
    
        Label(self.frameMain,text = "OUBLIER MOT DE PASSE",font=("Helvetica", 14)).pack(pady=(20,0),side=TOP)
        #Affichage Nom Joueur 
        Label(self.frameMain,text = "Nom Joueur",justify=LEFT,font=("Helvetica", 12)).pack(padx= 20 , pady=(40,0),side=TOP,anchor=W)
        self.nomJoueurEntry = Entry(self.frameMain,width=30,textvariable=self.nomJoueur)
        self.nomJoueurEntry.pack(padx=20,side=TOP,anchor=W)
        
        #Affichage Questions 
        Label(self.frameMain,text = self.questionA ,font=("Helvetica", 12)).pack(padx= 20 , pady=(25,0),side=TOP,anchor = W)
        self.reponseQuestionAEntry= Entry(self.frameMain,width=30,textvariable=self.reponseQuestionA)
        self.reponseQuestionAEntry.pack(padx= 20,anchor = W)
        Label(self.frameMain,text = self.questionB ,font=("Helvetica", 12)).pack(padx= 20 , pady=(25,0),side=TOP,anchor = W)
        self.reponseQuestionBEntry = Entry(self.frameMain,width=30,textvariable=self.reponseQuestionB)
        self.reponseQuestionBEntry.pack(padx= 20,anchor = W)
        
        #Affichage Password 
        Label(self.frameMain,text = "Nouveau Password" ,font=("Helvetica", 12)).pack(padx= 20 ,pady=(25,0),side=TOP,anchor = W)
        self.nouveauPassword = Entry(self.frameMain,width=30,show="*",textvariable=self.password1)
        self.nouveauPassword.pack(padx= 20,anchor = W)
        Label(self.frameMain,text = "Repetez Password" ,font=("Helvetica", 12)).pack(padx= 20 ,pady=(25,0),side=TOP,anchor = W)
        self.repetepassword = Entry(self.frameMain,width=30,show="*",textvariable=self.password2)
        self.repetepassword.pack(padx= 20,anchor = W)
        
        self.reponseLabel=Label(self.frameMain,textvariable = self.reponse,font=("Helvetica", 12))
        self.reponseLabel.pack(side=TOP,pady=25)

        #Button confirme
        self.confirm = Button ( self.frameMain , text = "Confirmer",font=("Helvetica", 14),command = self.confirmInfo)
        self.confirm.pack(pady=10)
        self.confirm.config(state='disabled')
        
        self.frameMain.pack()


    def confirmInfo(self):
        print (self.reponseQuestionA.get(), self.reponseQuestionB.get())
        if ( (self.reponseQuestionA.get() != self.dto.getInfo('ReponseA')) and (self.reponseQuestionB.get() != self.dto.getInfo('ReponseB'))) :
            self.reponse.set("NOPE")
            self.reponseLabel.config(fg="red")
        else: 
            self.reponse.set("BRAVO")
            self.reponseLabel.config(fg="green")
            
            hashedPass = bcrypt.hashpw(self.password2.get(), bcrypt.gensalt())
            self.dto.set("Password",hashedPass)
            print(self.dto.getInfo("Password"))
            self.dao.update(self.dto)


        
        
