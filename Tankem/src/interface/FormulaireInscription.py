from Tkinter import *
import tkMessageBox
import Tkinter as tk
from DTOJoueur import DTOJoueur#
import bcrypt
from DAOOracleUser import DAOOracleUser



class FormulaireInscription():
    
    def __init__(self,):
           self.root = Tk()
           self.root.title("Inscription")
           self.setUpMenu()
           self.root.mainloop()

      
    def setUpMenu(self):
        #Get les questions de notre BD selon nom joueur (hardcoder pour l'instant!
        self.frameMain= Frame(self.root,width=600,height = 600)
        self.frameMain.pack_propagate(0)
        
        #Creation des stringvar ou les donnes vont etre rempli
        self.nomJoueur = tk.StringVar(self.root)
        self.nom = tk.StringVar(self.root)
        self.prenom =  tk.StringVar(self.root)
        self.questionA = tk.StringVar(self.root)
        self.questionB = tk.StringVar(self.root)
        self.reponseQuestionA = tk.StringVar(self.root)
        self.reponseQuestionB = tk.StringVar(self.root)
        self.password1 = tk.StringVar(self.root)
        self.password2 = tk.StringVar(self.root)
        self.reponse=tk.StringVar(self.root)
        self.securitePassword=tk.StringVar(self.root)
    
 
        Label(self.frameMain,text = "INSCRIPTION",font=("Helvetica", 14)).pack(pady=(40,0))
        self.reponseLabel= Label(self.frameMain,textvariable = self.reponse,font=("Helvetica", 14))
        self.reponseLabel.pack()

        
        self.frameTitre= Frame(self.frameMain,width=200,height = 425)
        self.frameTitre.pack_propagate(0)
        
        self.frameChamp= Frame(self.frameMain,width=300,height = 425)
        self.frameChamp.pack_propagate(0)
        

        #Affichage Nom Joueur 
        Label(self.frameTitre,text = "Nom Joueur",font=("Helvetica", 12)).pack(side=TOP,anchor = E)
        self.nomJoueurEntry = Entry(self.frameChamp,width=40,textvariable=self.nomJoueur)
        self.nomJoueurEntry.pack(side=TOP,anchor = W)
                
        #Affichage Nom Prenom 
        Label(self.frameTitre,text = "Nom" ,font=("Helvetica", 12)).pack(pady=(20,0),side=TOP,anchor = E)
        self.nomEntry= Entry(self.frameChamp,width=40,textvariable=self.nom)
        self.nomEntry.pack(pady=(25,0),side=TOP,anchor = W)
        
        Label(self.frameTitre,text="Prenom" ,font=("Helvetica", 12)).pack(pady=(20,0),side=TOP,anchor = E)
        self.prenomEntry = Entry(self.frameChamp,width=40,textvariable=self.prenom)
        self.prenomEntry.pack(pady=(22,0),side=TOP,anchor = W)
    
        #Affichage Questions 
        Label(self.frameTitre,text = "Question Secrete 1" ,font=("Helvetica", 12)).pack(pady=(20,0),side=TOP,anchor = E)
        self.questionAEntry= Entry(self.frameChamp,width=40,textvariable=self.questionA)
        self.questionAEntry.pack(pady=(25,0),side=TOP,anchor = W)
        
        Label(self.frameTitre,text = "Reponse Question 1" ,font=("Helvetica", 12)).pack(pady=(20,0),side=TOP,anchor = E)
        self.reponseQuestionAEntry= Entry(self.frameChamp,width=40,textvariable=self.reponseQuestionA)
        self.reponseQuestionAEntry.pack(pady=(25,0),side=TOP,anchor = W)
        
        Label(self.frameTitre,text="Question Secrete 2" ,font=("Helvetica", 12)).pack(pady=(20,0),side=TOP,anchor = E)
        self.questionBEntry = Entry(self.frameChamp,width=40,textvariable=self.questionB)
        self.questionBEntry.pack(pady=(25,0),side=TOP,anchor = W)
        
        Label(self.frameTitre,text = "Reponse Question 2" ,font=("Helvetica", 12)).pack( pady=(20,0),side=TOP,anchor = E)
        self.reponseQuestionBEntry= Entry(self.frameChamp,width=40,textvariable=self.reponseQuestionB)
        self.reponseQuestionBEntry.pack(pady=(25,0),side=TOP,anchor = W)

        #Affichage Password 
        self.okPassword = False
        self.securitePassword.set("Password") 
        self.labelPassword = Label(self.frameTitre,textvariable =  self.securitePassword ,font=("Helvetica", 12))
        self.labelPassword.pack(pady=(20,0),side=TOP,anchor = E)
        self.tipPassword = Label (self.frameChamp,font=("Helvetica", 7),text="Devrais contenir chiffre, Majuscule et charactere special")
        self.tipPassword.pack(pady=(12,0),anchor=W)
        self.nouveauPassword = Entry(self.frameChamp,width=40,show="*",textvariable=self.password1)
        self.nouveauPassword.pack(side=TOP,anchor = W)
        Label(self.frameTitre,text = "Repetez Password" ,font=("Helvetica", 12)).pack(pady=(20,0),side=TOP,anchor = E)
        self.repetepassword = Entry(self.frameChamp,width=40,show="*",textvariable=self.password2)
        self.repetepassword.pack(pady=(25,0),side=TOP,anchor = W)
        
        self.frameTitre.pack(side=LEFT)
        self.frameChamp.pack(side=RIGHT,fill=X)
        
        #Button confirme
        self.confirm = Button (self.frameMain , text = "S'inscrire",font=("Helvetica", 14),command = self.confirmInfo)
        self.confirm.pack(side=BOTTOM,pady=(0,20))
        self.confirm.config(state='disabled')

        self.frameMain.pack()
        

        #Fonction qui active le bouton de confirmation quand tous les champs sont remplis
        def entryRempli(*args):
            entryA=self.nomJoueur.get()
            entryB=self.nom.get()
            entryC=self.prenom.get()
            entryD=self.questionA
            entryE=self.reponseQuestionA.get()
            entryF=self.questionB
            entryG=self.reponseQuestionB.get()
            entryH=self.password1.get()
            entryI=self.password2.get()
    
            if entryA and entryB and entryC and entryD and entryE and entryF and entryG and entryH and entryI:
                self.confirm.config(state='normal')
            else:
                self.confirm.config(state='disabled')
                
        #affecte la fonction entryRempli a mes strinvar
        self.nomJoueur.trace("w", entryRempli)
        self.reponseQuestionA.trace("w",entryRempli)
        self.reponseQuestionB.trace("w", entryRempli)
        self.password1.trace("w", entryRempli)
        self.password2.trace("w", entryRempli)
            
        #Verifie la securite du mdp
        def securePassword(*args):
            password = self.password1.get()
            
            #longueur password
            tropCourt = len(password) < 9
            #Chiffre
            aucunChiffre = re.search(r"\d", password) is None
            #Majuscule
            aucunMaj = re.search(r"[A-Z]", password) is None
            # Symbole
            aucunSymbole = re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', password) is None
            
            # overall result
            password_ok = not (aucunChiffre or aucunMaj  or aucunSymbole )
            
            if tropCourt == True:
                self.securitePassword.set("Password (UNSAFE)")
                self.labelPassword.config(fg="red")
            elif password_ok == False:
                self.securitePassword.set("Password (Medium)")
                self.labelPassword.config(fg="orange")
                self.okPassword = True
            else :
                self.securitePassword.set("Password (Safe)")
                self.labelPassword.config(fg="green")
                self.okPassword = True             
        self.password1.trace("w", securePassword)



    

    def confirmInfo(self):
        dto = DTOJoueur()
        dao = DAOOracleUser()
        self.reponse.set("Membre Creer")
        self.reponseLabel.config(fg="green")
        self.dictUser = {}
        self.dictUser['Prenom'] = self.prenom.get()
        self.dictUser['Nom'] =  self.nom.get()
        self.dictUser['Username'] = self.nomJoueur.get()
        self.dictUser['Couleur'] = '160,160,160'
        self.dictUser['QuestionA'] =  self.questionA.get()
        self.dictUser['QuestionB'] = self.questionB.get()
        self.dictUser['ReponseA'] = self.reponseQuestionA.get()
        self.dictUser['ReponseB'] = self.reponseQuestionB.get()
        hashedPass = bcrypt.hashpw(self.password1.get(), bcrypt.gensalt())
        self.dictUser['Password'] = hashedPass
        dto.setInfo(self.dictUser)
        dao.create(dto)
        return(dto)

#FormulaireInscription()