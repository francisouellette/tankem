from Tkinter import *
import tkMessageBox
import Tkinter as tk
from OublieMdp import OublieMdp
from FormulaireInscription import FormulaireInscription
import os
import tkFont
from tkColorChooser import *
import math

from DTOJoueur import DTOJoueur
from DAOOracleUser import DAOOracleUser
from OublieMdp import OublieMdp 
from daoOracleMap import daoOracleMap

class PagePerso():
    
    def __init__(self,username):
           self.root = Tk()
           self.user = username
           self.setUpPagePerso()
           self.root.title("PagePerso")
           self.root.mainloop()
    
    #redirection vers la page oublie mot de passe  
    
    def changerMDP(self):    
        OublieMdp(self.dto.getInfo("Username"))
    
    def deco(self):
        self.root.destroy()

    
    def setUpPagePerso(self):
        helv36 = tkFont.Font(family='Helvetica',size=10, weight='bold')

        #Normalement on get ca dans le dto
        self.dto = DTOJoueur()
        self.dao = DAOOracleUser()
        self.daoMap = daoOracleMap()
        self.dto = self.dao.readUserInfo(self.user)
        self.dtoListMap = DTOJoueur()
        
        
        self.username = self.dto.getInfo("Username")
        self.nom = self.dto.getInfo("Nom")
        self.prenom = self.dto.getInfo("Prenom")
        self.dtoListMap = self.daoMap.getListeMapUser(self.username)
        self.listMaps = self.dtoListMap.getInfo("Nom Level")
        self.listFavsdto = self.dao.getInfoFavoris(self.username)
        self.listFavs = self.listFavsdto.getInfo("NomMap")
        print(self.listFavs)
        #cree le frame de la page
        self.frameMain= Frame(self.root,width=600,height = 600)  
        self.frameMain.pack_propagate(0)   
        
        ##########################################
        #Haut de la page (titre + bouton logout
        ##########################################
        Label(self.frameMain,text = "Page Personnelle",font=("Helvetica", 14)).pack(pady=(20,0),side=TOP)

        self.boutonLogout = Button ( self.frameMain , text = "Deconnecter",font=("Helvetica", 14),command = self.deco )



        self.boutonLogout['font'] = helv36
        self.boutonLogout.pack(pady=(10,0),padx=10,side=TOP,anchor=E)
        
        ##################################################    
        #Info User (Nom prenom username + change password
        ##################################################
        self.frameInfo= Frame(self.frameMain,width=600,height = 105)
        self.frameInfo.pack_propagate(0)
        
        ####Inclus info user dans notre affiche de label
        self.usernameLabel = "Username : " + self.username
        self.nomLabel = 'Nom : ' + self.nom
        self.prenomLabel ="Prenom : "+self.prenom
        self.Ligne = "_____________________________________________________________"

        Label(self.frameInfo,text = self.nomLabel ,font=("Helvetica", 10)).pack(padx= 20 ,side=TOP,anchor = W)
        Label(self.frameInfo,text = self.prenomLabel ,font=("Helvetica", 10)).pack(padx= 20 , pady=(5,0),side=TOP,anchor = W)
        Label(self.frameInfo,text = self.usernameLabel ,font=("Helvetica", 10)).pack(padx= 20 , pady=(5,0),side=TOP,anchor = W)
        

        self.boutonChangeMdp = Button ( self.frameInfo ,text = "Changer mot de passe",font=("Helvetica", 14),command = self.changerMDP )



        self.boutonChangeMdp['font'] = helv36        
        self.boutonChangeMdp.pack(padx=10,side=BOTTOM,anchor=E)

        self.frameInfo.pack(side=TOP)
        Label(self.frameMain,text = self.Ligne ,font=("Helvetica", 12)).pack(padx=20 , pady=(2,0),side=TOP,anchor = W)

        ##################################################    
        #Les listes (niveaux favoris, creer et armorie
        ##################################################
        self.frameListe= Frame(self.frameMain,width=600,height = 150)
        self.frameListe.pack_propagate(0)
        
        ###Niveaux favoris
        self.frameListeFavoris=Frame(self.frameListe,width=50,height = 50,bg='gray')
        title=Label(self.frameListeFavoris,text="Niveaux Favoris")
        title.pack(side=TOP)
        scrollbar = tk.Scrollbar(self.frameListeFavoris, orient="vertical")
        lbFavoris = tk.Listbox(self.frameListeFavoris, width=25, height=20, yscrollcommand=scrollbar.set)
        scrollbar.config(command=lbFavoris.yview)
        scrollbar.pack(side=RIGHT, fill="y")
        lbFavoris.pack(side="left")
        ###C'est ici qu'on rentre les valeurs dans notre liste Niveaux favoris
        for i in self.listFavs:
            lbFavoris.insert("end", "%s" % i)
        self.frameListeFavoris.pack(side=LEFT,padx=(25,10))
            
        ###Niveaux creer
        self.frameListeCreer=Frame(self.frameListe,width=150,height = 105,bg='gray')
        title=Label(self.frameListeCreer,text="Niveau Creer")
        title.pack(side=TOP)
        scrollbar = tk.Scrollbar(self.frameListeCreer, orient="vertical")
        lbCreer = tk.Listbox(self.frameListeCreer, width=25, height=20, yscrollcommand=scrollbar.set)
        scrollbar.config(command=lbCreer.yview)
        scrollbar.pack(side=RIGHT, fill="y")
        lbCreer.pack(side="left")
        ###C'est ici qu'on rentre les valeurs dans notre liste Niveaux creer
        for i in self.listMaps:
            lbCreer.insert("end", "%s" % i)
        self.frameListeCreer.pack(side=LEFT,padx=10)
        
        ###Armorie
        self.frameListeArmorie=Frame(self.frameListe,width=150,height = 105,bg='gray')
        title=Label(self.frameListeArmorie,text="Armurie")
        title.pack(side=TOP)
        scrollbar = tk.Scrollbar(self.frameListeArmorie, orient="vertical")
        lbArmorie = tk.Listbox(self.frameListeArmorie, width=25, height=20, yscrollcommand=scrollbar.set)
        scrollbar.config(command=lbArmorie.yview)
        scrollbar.pack(side=RIGHT, fill="y")
        lbArmorie.pack(side="left")
        ###C'est ici qu'on rentre les valeurs dans notre liste Armorie
        listArmorie = ['normal']
        for i in listArmorie:
            lbArmorie.insert("end", "%s" % i)
        self.frameListeArmorie.pack(side=LEFT,padx=10)

        self.frameListe.pack(pady=10)
        
        Label(self.frameMain,text = self.Ligne ,font=("Helvetica", 12)).pack(padx=20 , pady=(2,0),side=TOP,anchor = W)
        
        
        ##################################################    
        #Choisir Couleur (Tank)
        ##################################################
        self.frameCouleur= Frame(self.frameMain,width=350,height = 200)
        self.frameCouleur.pack_propagate(0)
        canvas = Canvas(self.frameCouleur,width= 200,height = 200)
        couleurRGB = self.dto.getInfo("Couleur").split(",")
        print(couleurRGB)
        self.mycolor = '#%02x%02x%02x' %(int(couleurRGB[0]),int(couleurRGB[1]),int(couleurRGB[2]))  # set your favourite rgb color
        
        
        def getColor():
            color = askcolor()
            id = canvas.create_rectangle(25, 100, 175,130,fill=color[1])
            id = canvas.create_rectangle(70, 60, 140, 100,fill=color[1])
            id = canvas.create_rectangle(15, 75, 70, 85,fill=color[1])
            canvas.update()
            self.mycolor  = color[0]
            self.color = str(self.mycolor[0])+","+ str(self.mycolor[1])+","+ str(self.mycolor[2])
            self.dto.set("Couleur",self.color)
            self.dao.updateColor(self.dto)
           
        Label(self.frameCouleur,text="Choisir Couleur",font=("Helvetica", 12)).pack(side=TOP,pady=(10,0))
        Button(self.frameCouleur,text='Select Color', command=getColor).pack(side=LEFT)
    
        id = canvas.create_rectangle(25, 100, 175,130,fill=self.mycolor)
        id = canvas.create_rectangle(70, 60, 140, 100,fill=self.mycolor)
        id = canvas.create_rectangle(15, 75, 70, 85,fill=self.mycolor)

        canvas.pack(side = RIGHT,padx=10)
        
        self.frameCouleur.pack()
        self.frameMain.pack() 

        
  
        
#PagePerso("test1")