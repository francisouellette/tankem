from Tkinter import *
import tkMessageBox
import Tkinter as tk
from OublieMdp import OublieMdp
from FormulaireInscription import FormulaireInscription
import os
from DAOOracleUser import DAOOracleUser
from DTOJoueur import DTOJoueur
from PagePerso import PagePerso
import bcrypt




class Login():
    
    def __init__(self):
           self.root = Tk()
           self.setUpLogin()
           self.root.title("Login")
           self.root.mainloop()
    
    #redirection vers la page oublie mot de passe      
    def callback(self,event):
        
            if(self.UserEntrer.get() == ""):
                tkMessageBox.showinfo("ERREUR", "Vous devez entrer un Username!")
            else:
                try:
                    OublieMdp(self.UserEntrer.get())
                except: 
                    tkMessageBox.showinfo("ERREUR", "Ce Username n'existe pas")
        
            
    #redirection vers la page inscription
    def inscription(self):
        FormulaireInscription()
    
    def mail(self):
        os.startfile("outlook") 
        
    def login(self):
        dao = DAOOracleUser()
        dto = DTOJoueur()
        dtoLog = DTOJoueur()
        
        dtoLog = dao.readUserMDP(self.UserEntrer.get())
        try:
            hashedPass = dtoLog.getInfo('Password')
            if(bcrypt.hashpw(self.PasswordEntrer.get(), hashedPass) == hashedPass):
                dto = dao.userLogin(self.UserEntrer.get(),hashedPass)     
                PagePerso(dto.getInfo("Username"))              
            else :
                 tkMessageBox.showinfo("ERREUR", "Le Username et le mot de passe ne concorde pas  !")
        except:
            tkMessageBox.showinfo("ERREUR", "Ce Username n'existe pas !")
            
        
    
    def setUpLogin(self):
        #Get les questions de notre BD selon nom joueur (hardcoder pour l'instant!
        self.Username = "Username :" # REPONSE: vagin
        self.Password = "Password :"  # REPONSE: drago 
        self.ForgotPassword = "Forgot Password?"
        self.Ligne = "_________________________"
        self.NewUser = "New User"
        
        
        
        #cree le frame de la page
        self.frameMain= Frame(self.root,width=250,height = 575)
        self.frameMain.pack_propagate(0)
        
        #Creation des stringvar ou les donnes vont etre rempli
        self.reponseQuestionA = tk.StringVar(self.root)
        self.reponseQuestionB = tk.StringVar(self.root)
        self.reponse=tk.StringVar(self.root)

     
        #affecte la fonction entryRempli a mes strinvar
        Label(self.frameMain,text = "Sign In",font=("Helvetica", 14)).pack(pady=(20,0),side=TOP)
    
        
        #Affichage Questions 
        Label(self.frameMain,text = self.Username ,font=("Helvetica", 12)).pack(padx= 20 , pady=(25,0),side=TOP,anchor = W)
        self.UserEntrer= Entry(self.frameMain,width=30,textvariable=self.reponseQuestionA)
        self.UserEntrer.pack(padx= 20,anchor = W)
        
        Label(self.frameMain,text = self.Password ,font=("Helvetica", 12)).pack(padx= 20 , pady=(25,0),side=TOP,anchor = W)
        self.PasswordEntrer = Entry(self.frameMain,width=30,textvariable=self.reponseQuestionB,show="*")
        self.PasswordEntrer.pack(padx= 20,anchor = W)
        

        self.signin = Button ( self.frameMain , text = "Sign In",font=("Helvetica", 14),command = self.login )


        self.signin.pack(pady=(20,0))
        
        self.link = Label(self.frameMain, text=self.ForgotPassword, fg="blue", cursor="hand2")
        self.link.pack(padx = 5, pady=(20,0),anchor = W)
        self.link.bind("<Button-1>", self.callback)
        
        Label(self.frameMain,text = self.Ligne ,font=("Helvetica", 12)).pack(padx= 20 , pady=(2,0),side=TOP,anchor = W)
        Label(self.frameMain,text = self.NewUser ,font=("Helvetica", 12)).pack(padx= 20 , pady=(2,0),side=TOP,anchor = W)
        
        self.signup = Button ( self.frameMain , text = "Sign Up",font=("Helvetica", 14),command = self.inscription )
        self.signup.pack(padx = 5, pady= 20,anchor = W)
        
        self.support = Button ( self.frameMain , text = "Support",font=("Helvetica", 14),command = self.mail )
        self.support.pack(padx = 5, pady= 1, anchor = W        )
        
        
        self.frameMain.pack()
        
    
        
        #Button confirme
        
#Login()
