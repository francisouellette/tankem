
from Tkinter import *
import tkMessageBox
import Tkinter as tk
#import Image, ImageTk
from math import floor
from mapEditorDTO import MapEditorDTO
from daoOracleMap import daoOracleMap
from direct.task.Task import done
from DTOJoueur import DTOJoueur
from DAOOracleUser import DAOOracleUser
import bcrypt


class run():
    def __init__(self):
        self.root = Tk()
        self.newValeurX =6
        self.newValeurY =6

        self.newTempsMin = 0
        self.newTempsMax = 0
        self.matrice = []
        self.listeCaseCouleur = []
        self.couleurChoisi = StringVar()
        self.setUpMenu()
        self.root.mainloop()
        try:
            self.root.withdraw()
        except:
            pass

        
    def setUpMenu(self):
        ###CREATION DE NOS TROIS FRAMES 
        self.frameBouton= Frame(self.root,width=350,height=500)
        self.frameBouton.grid_propagate(False)
        self.canvas = Canvas(self.root,width=500,height=500,)
        self.canvas.grid_propagate(False)
        self.frameOptionsCanvas= Frame(self.root,width=350,height=500)
        self.frameOptionsCanvas.grid_propagate(False)
        ###################################################################################################
        # OPTIONS DU LEVEL (FRAME BOUTON)
        ##################################################################################################
        #############
        #OPTION GROSSEUR X (FRAME BOUTON)
        boutonMoinsX = Button(self.frameBouton,  fg='green',text="DOWN", command = lambda:self.modifieValeur("grosseurX",-1))
        boutonMoinsX.grid(row=0,column=0,padx=15,pady=10)
        
        Label(self.frameBouton,text="Grosseur X = ").grid(row=0,column=1,padx=15)
        
        #STRINGVAR dans le frame bouton pour modifier 
        #automatiquement lorsqu'on appuie sur les fleches
        self.stringnewValeurX = StringVar() 
        self.stringnewValeurX.set(self.newValeurX)#
        grosseurX = Label(self.frameBouton,textvariable=self.stringnewValeurX)
        grosseurX.grid(row=0,column=2,padx=15)
        
        boutonPlusX = Button(self.frameBouton, text="UP", fg="green",command = lambda:self.modifieValeur("grosseurX",1))
        boutonPlusX.grid(row=0,column=3,padx=20)
        
        ####################
        #OPTION GROSSEUR Y (FRAME BOUTON)
        boutonMoinsY = Button(self.frameBouton,  text="DOWN",  fg="green", command = lambda:self.modifieValeur("grosseurY",-1))
        boutonMoinsY.grid(row=1,column=0,pady=10)
        
        labelY = Label(self.frameBouton,text="Grosseur Y = ")
        labelY.grid(row=1,column=1)
        
        self.stringValeurY = StringVar()
        self.stringValeurY.set(self.newValeurY)
        self.grosseurY = Label(self.frameBouton,textvariable=self.stringValeurY)
        self.grosseurY.grid(row=1,column=2)
        
        boutonPlusY = Button(self.frameBouton, text="UP", fg="green", command = lambda:self.modifieValeur("grosseurY",1))
        boutonPlusY.grid(row=1,column=3)
        
        ##############
        #OPTION TEMPS MIN(FRAME BOUTON)
        boutonMoinsnewTempsMin = Button(self.frameBouton,  text="DOWN",  fg="green", command = lambda:self.modifieValeur("valeurMin",-1))
        boutonMoinsnewTempsMin.grid(row=2,column=0,pady=10)
        
        labelnewTempsMin = Label(self.frameBouton,text="Temps Min = ")
        labelnewTempsMin.grid(row=2,column=1)
       
        self.stringnewTempsMin = StringVar()
        self.stringnewTempsMin.set(self.newTempsMin)
        self.grosseurnewTempsMin = Label(self.frameBouton,textvariable= self.stringnewTempsMin)
        self.grosseurnewTempsMin.grid(row=2,column=2)
        
        boutonPlusnewTempsMin = Button(self.frameBouton, text="UP", fg="green", command = lambda:self.modifieValeur("valeurMin",1))
        boutonPlusnewTempsMin.grid(row=2,column=3)
        
        ##############
        #OPTION TEMPS MAX(FRAME BOUTON)
        boutonMoinsnewTempsMax = Button(self.frameBouton,  text="DOWN",  fg="green", command = lambda:self.modifieValeur("valeurMax",-1))
        boutonMoinsnewTempsMax.grid(row=3,column=0,pady=10)
        
        labelnewTempsMax = Label(self.frameBouton,text="Temps Max = ")
        labelnewTempsMax.grid(row=3,column=1)
        
        self.stringnewTempsMax = StringVar()
        self.stringnewTempsMax.set(self.newTempsMax)
        self.grosseurnewTempsMax = Label(self.frameBouton,textvariable=self.stringnewTempsMax)
        self.grosseurnewTempsMax.grid(row=3,column=2)
        
        boutonPlusnewTempsMax = Button(self.frameBouton, text="UP", fg="green",command = lambda:self.modifieValeur("valeurMax",1))
        boutonPlusnewTempsMax.grid(row=3,column=3)
        ##########
        #OPTION STATUS (FRAME BOUTON)
        labelStatut = Label(self.frameBouton,text="Statut = ")
        labelStatut.grid(row=4,column=0,pady=10)
        
        self.spinBoxStatut = Spinbox(self.frameBouton, values=("Public","Prive","Inactif"), width=13)
        self.spinBoxStatut.grid(row=4,column = 1)
        
        #############
        #Entry NomLevel (FRAME BOUTON)
        Label(self.frameBouton,text="Nom Niveau ",pady=30).grid(row=5,column=0)
        self.nomLevel = StringVar()
        self.nomLevel.trace_variable("w", self.max20char)
        self.nomLevelEntry = Entry(self.frameBouton,textvariable = self.nomLevel)
        self.nomLevelEntry.grid(row=5,column=1)

        
        ###################################################################################################
        # TYPES DE MUR 
        ##################################################################################################
        Label(self.frameOptionsCanvas,text="Types de mur",font=("Purisa", 20),pady=20).grid(row=0,column=0)
        ###########
        #Plancher
        ########### 
        self.rb1 = tk.Radiobutton(self.frameOptionsCanvas, variable=self.couleurChoisi, value="#6F532A",text="Plancher",font=("Purisa", 12),fg="#6F532A")
        self.rb1.grid(row=1,column=0,sticky="w")
        self.rb1.select()
        ######
        #Mur
        #######
        self.rb2 = tk.Radiobutton(self.frameOptionsCanvas,  variable=self.couleurChoisi, value="purple",text="Mur",font=("Purisa", 12),fg="purple")
        self.rb2.grid(row=2,column=0,sticky="w")

        ######
        #Mur Anime Up 
        ######
        Label(self.frameOptionsCanvas,).grid(row=3,column=0,sticky="w")
        self.rb3 = tk.Radiobutton(self.frameOptionsCanvas, variable=self.couleurChoisi, value="green",text="Mur Anime Up",font=("Purisa", 12),fg="green")
        self.rb3.grid(row=3,column=0,sticky="w")

        ######
        #Mur Anime Down  
        ######   
        self.rb4 = tk.Radiobutton(self.frameOptionsCanvas,  variable=self.couleurChoisi, value="red",text="Mur Anime Down",font=("Purisa", 12),fg="red")
        self.rb4.grid(row=4,column=0,sticky="w")
        ########
        #Bouton DONE
        ########
        Button(self.frameOptionsCanvas,text="DONE", command = self.confirmeLogin,width=45,height=2).grid(row=5, column=0,pady=100)
        
        ##############
        self.frameBouton.pack(side=LEFT,padx=30,pady=10)
        self.canvas.pack(side=LEFT,padx=25,pady=40)
        self.canvas.bind("<Button-1>", self.clickCanvas)
        self.frameOptionsCanvas.pack(side=LEFT,pady=10)
        self.initMatrice()
        

    
    def modifieValeur(self, element, valeur):
        #######LORSQUE USER MODIFIE LA GRANDEUR (X ET Y) ET LE TEMPS (MIN ET MAX)
        if element == "grosseurX":
            self.newValeurX = self.newValeurX + valeur
            if self.newValeurX<6 : self.newValeurX = 6 #MIN = 6 
            elif self.newValeurX>12 : self.newValeurX = 12 #MAX = 12 
            self.stringnewValeurX.set(self.newValeurX)
            self.initMatrice()
            
        elif element == "grosseurY":
            self.newValeurY = self.newValeurY + valeur
            if self.newValeurY<6 : self.newValeurY = 6 #MIN = 6 
            elif self.newValeurY>12 : self.newValeurY = 12 #MAX = 12
            self.stringValeurY.set(self.newValeurY)
            self.initMatrice()

        elif element == "valeurMin":
            self.newTempsMin= self.newTempsMin + valeur
            if self.newTempsMin<0 : self.newTempsMin = 0 #Minimum doit etre > 0 
            if self.newTempsMin >= self.newTempsMax: self.newTempsMax = self.newTempsMin #Minimum doit etre < MAXIMUM
            self.stringnewTempsMin.set(self.newTempsMin)
            self.stringnewTempsMax.set(self.newTempsMax)

        elif element == "valeurMax":
            self.newTempsMax= self.newTempsMax + valeur
            if self.newTempsMax<0 : self.newTempsMax = 0 # Max doit etre > 0 
            if self.newTempsMax <= self.newTempsMin: self.newTempsMin = self.newTempsMax #Maximum doit etre > Minimum
            self.stringnewTempsMin.set(self.newTempsMin)
            self.stringnewTempsMax.set(self.newTempsMax)
            
    def initMatrice(self): 
        self.matrice= [[0] * self.newValeurX for i in range(self.newValeurY)]     
        self.canvas.delete("all")    
        sizeXcanvas = 500 
        sizeYcanvas = 500
        
        self.matrice = [[None]*self.newValeurX for i in range(self.newValeurY)] 
        
        for i in range(self.newValeurY):
            for j in range(self.newValeurX):
                initx = floor(sizeXcanvas / self.newValeurX * j ) # <-- Code trouver online pour afficher matrice dans un espace predeterminer 
                inity = floor(sizeYcanvas / self.newValeurY * i) # 
                endx = floor(sizeXcanvas / self.newValeurX * (j+1)) # 
                endy = floor(sizeYcanvas / self.newValeurY * (i+1)) # 
                self.matrice[i][j] = self.canvas.create_rectangle(initx, inity, endx, endy, fill="#6F532A",tag="plancher")


    def clickCanvas(self,event):
        #lorsque le user click sur canvas pour changer la couleur 
        #get l'endroit cliquer par user (x,y)
        cx = self.canvas.canvasx(event.x)
        cy = self.canvas.canvasy(event.y)
        #canvas detecte le id de l'item cliquer 
        cid = self.canvas.find_closest(cx,cy)[0]
        #On enregistre un tag selon la nouvelle couleur choisi 
        if self.couleurChoisi.get() == "#6F532A":
            tagType ="plancher"
        elif self.couleurChoisi.get() == "purple":
            tagType ="mur"
        elif self.couleurChoisi.get() == "green":
            tagType ="up"
        else :  tagType ="down"
        ##change la couleur du carre choisi et lui assigne un tag avec le type (plancher, mur, up ,down) 
        self.canvas.itemconfigure(cid, fill=self.couleurChoisi.get(),tag=tagType)
        
    def confirmeLogin(self):
            self.toplevel = Toplevel()
            Label(self.toplevel,text ="username").pack(pady=10,padx=10)
            self.entry1 = Entry(self.toplevel)
            self.entry1.pack(pady=10,padx=10)  
            Label(self.toplevel,text="password").pack(pady=10,padx=10)
          
            self.entry2 = Entry(self.toplevel,show="*")
            self.entry2.pack(pady=10,padx=10)
            Button(self.toplevel,command=self.testLogin,text="DONE",width=45,height=2).pack()
    
    def envoyerValeurs(self):
        dto = MapEditorDTO()
        dao = daoOracleMap()
        self.listeCaseCouleur = []
        self.nbValeurs = (self.newValeurX * self.newValeurY) # determine le nombre de case sur screen 
        #ce code sert a trouver l'item au top left de notre damier. il servira de nous pointer le debut de notre tableau
        cidCourant = self.canvas.find_closest(0,0)[0] 
   
        for i in range(self.nbValeurs):
            # numero d'item au top left + nb item = nb total
             self.listeCaseCouleur.append([i,self.canvas.gettags(i+cidCourant)[0]])
             
             
        matriceJeu =  [[0 for y in range(self.newValeurY)] for x in range(self.newValeurX)]  
        compteur = 0
        for x in range(self.newValeurX):
            for y in range(self.newValeurY):
                matriceJeu[x][y] = self.listeCaseCouleur.__getitem__(compteur)[1]
                compteur+=1

        dictLevel = {}
        dictLevel['Nom Level'] = self.nomLevel.get()
        dictLevel['Users'] = self.dtoUser.getInfo('Username')
        dictLevel['Size X'] = self.stringnewValeurX.get()
        dictLevel['Size Y'] = self.stringValeurY.get()
        dictLevel['Temps Min'] = self.stringnewTempsMin.get()
        dictLevel['Temps Max'] = self.stringnewTempsMax.get()
        dictLevel['Statut'] =  self.spinBoxStatut.get()
        dictLevel['Matrice Jeu'] = matriceJeu
        dto.setInfo(dictLevel)
        dao.create(dto)
        self.root.destroy()
        return(dto)

    def testLogin(self):  
        self.username = self.entry1.get() 
        self.password = self.entry2.get()
        self.login = False
        self.daoUser = DAOOracleUser()
        self.dtoUser = DTOJoueur()
        dtoLog = DTOJoueur()
 
        dtoLog = self.daoUser.readUserMDP(self.username)
        try:
            hashedPass = dtoLog.getInfo('Password')
            if(bcrypt.hashpw(self.password, hashedPass) == hashedPass):
                
                self.dtoUser = self.daoUser.userLogin(self.username,hashedPass)     
                self.envoyerValeurs()            
            else :
                 tkMessageBox.showinfo("ERREUR", "Le Username et le mot de passe ne concorde pas  !")
        except:
            tkMessageBox.showinfo("ERREUR", "Ce Username n'existe pas !")

        
    def max20char(self,*args):
        s = self.nomLevel.get()
        if len(s) > 20:
            self.nomLevel.set(s[:20])
            
       
        
#run()