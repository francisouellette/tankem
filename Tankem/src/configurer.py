import csv
from Tkinter import *
import tkMessageBox
import Tkinter as tk
from daoCSV import daoCSV
from _dto_Tankem import TankemBalanceDTO 
from daoOracle import daoOracle
import cx_Oracle as cx

login = 'hr/football@127.0.0.1'


class configurer():
    def __init__(self):
        self.root=Tk()      
        self.root.title("Gestionnaire CSV")
        self.root.withdraw()
        self.menuChoix()

    
    def menuChoix(self):
        self.b = Button(self.root, text="Choisir fichier CSV", command = self.menuCSV)
        self.b.place(relx=0.5, rely=0.5, anchor=CENTER)    

        
    def menuCSV(self):
        self.b.destroy()
        dCSV = daoCSV()
        self.dto= dCSV.read()
        self.dictionnaire = self.dto.getDic()
        self.variables = []
        self.entries = []
        Label(text = "Nom").grid(row=0, column=0,padx=5)
        Label(text = "Valeur courante").grid(row=0, column=1,padx=5)
        Label(text = "Minimum").grid(row=0, column=2,padx=5)
        Label(text = "Maximum").grid(row=0, column=3,padx=5)
        Label(text = "Nouvelle Valeur").grid(row=0, column=4,padx=5)
        i = 0
        for valeur in self.dictionnaire:
            try:
                con = cx.connect(login)
                cursor = con.cursor()
                cursor.execute("select min from Balance  WHERE NOM = '"+ valeur+"'")
                min= float(cursor.fetchall()[0][0])
                cursor.execute("select max from Balance WHERE NOM = '"+ valeur+"'")
                max = float(cursor.fetchall()[0][0])
            except:
                max = ""
                min = ""
            Label(text = valeur).grid(row=i+1, column=0,padx=5)
            Label(text = self.dto.getInfo(valeur)).grid(row=i+1, column=1,padx=5)
            Label(text = min).grid(row=i+1, column=2,padx=5)
            Label(text = max).grid(row=i+1, column=3,padx=5)
            va = StringVar()
            en = Entry(self.root, textvariable=va)
            en.grid(row=i+1, column=4)
            self.variables.append(va)
            self.entries.append(en)
            i+=1
        Button(text = "DONE",command=self.completerModification).grid(row=i+1,column=2,pady=20)
    
        
    def completerModification(self):
        dOracle = daoOracle()
        dCSV = daoCSV()
        dto = TankemBalanceDTO()
        i = 0
        for nomValeur in self.dictionnaire:
            newValeur = self.variables[i].get()
            if type(newValeur)is int:
                newValeur = float(newValeur)
            if newValeur is "":
                newValeur = self.dto.getInfo(nomValeur)
            dto.setInfo(nomValeur,newValeur)
            i += 1   
        self.root.withdraw()
        update=dOracle.update(dto)
        if not update : 
            self.root.deiconify()
        else:
            dCSV.create(dto)
            tkMessageBox.showinfo("YOUPPI","Modification reussi")
            self.root.destroy()


      
        
            
if __name__ =='__main__': 
    c = configurer()
    c.root.mainloop()