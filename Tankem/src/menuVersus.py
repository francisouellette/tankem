import sys
from panda3d.bullet import *
from panda3d.core import *
from direct.interval.IntervalGlobal import *
from math import *
from util import *
from entity import *

from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import Sequence
from panda3d.core import Point3
from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
from direct.showbase import DirectObject

from Tkinter import *
import tkMessageBox
import Tkinter as tk
import os
import tkFont
from tkColorChooser import *
import dependances
from interface import *
import bcrypt

 
class menuVs(ShowBase):
    
    def __init__(self,pandaBase):
       self.pandaBase = pandaBase
       self.accept("menuVS",self.afficheJeuMenu)
       self.mouse2cam =None
       
       
    def afficheJeuMenu(self):
        self.disableMouse()
        # Load the environment model.
        self.scene = loader.loadModel("environment")
        # Reparent the model to render.
        self.scene.reparentTo(render)
        # Apply scale and position transforms on the model.
        self.scene.setScale(0.25, 0.25, 0.25)
        self.scene.setPos(-8, 42, 0)
        # Add the spinCameraTask procedure to the task manager.
        
        
        
        self.green = VBase4(0 / 255.0, 255 / 255.0, 0 / 255.0,1)
        self.red = VBase4(255 / 255.0, 0 / 255.0, 0 / 255.0,1)

        taskMgr.add(self.spinCameraTask, "SpinCameraTask")

        btnScale = (0.18,0.18)
        text_scale = 0.12
        borderW = (0.04, 0.04)
        couleurBack = (0.243,0.325,0.121,1)
        separation = 0.5
        hauteur = 0.5
        
        """
        JOUEUR 1
        """''
        self.username1 = None
        self.Ready1=False
        self.textObject1 = OnscreenText(text = "Joueur 1", pos = (-0.9,0.8), 
        scale = 0.1,fg=(1,0.5,0.5,1),align=TextNode.ACenter,mayChange=1)
        self.entry1 = DirectEntry(text = "" ,scale=.05,command=self.username1Input
                        , numLines = 2,focus=1,pos = (-0.6,0,0.82))
        
        self.textObjectPass1 = OnscreenText(text = "Password", pos = (-0.9,0.6), 
        scale = 0.1,fg=(1,0.5,0.5,1),align=TextNode.ACenter,mayChange=1)
        self.entryPass1 = DirectEntry(text = "" ,scale=.05,command=self.password1Input,
         numLines = 2,focus=1,pos = (-0.6,0,0.62),obscured=1)
        self.textConfirm1 =  textObject = OnscreenText(text = "Not Ready", pos = (-0.9,0.4), 
        scale = 0.1,fg=(1,0.5,0.5,1),align=TextNode.ACenter,mayChange=1)
        #a = DirectEntry(text = "" ,scale=.05,command=getInfo,
        #initialText="Type Something", numLines = 2,focus=1,focusInCommand=clearText,
        #pos = (-separation,0,hauteur))
        
        """
        JOUEUR 2
        """
        self.username2=None
        self.Ready2=False

        self.textObject2 = OnscreenText(text = "Joueur 2", pos = (0.2,0.8), 
        scale = 0.1,fg=(1,0.5,0.5,1),align=TextNode.ACenter,mayChange=1)
        self.entry2 = DirectEntry(text = "" ,scale=.05,command=self.username2Input,
         numLines = 2,focus=1,pos = (0.5,0,0.82))
        
        self.textObjectPass2 = OnscreenText(text = "Password", pos = (0.2,0.6), 
        scale = 0.1,fg=(1,0.5,0.5,1),align=TextNode.ACenter,mayChange=1)
        self.entryPass2 = DirectEntry(text = "" ,scale=.05,command=self.password2Input,
        numLines = 2,focus=1,pos = (0.5,0,0.62),obscured=1)
        self.textConfirm2 =  textObject = OnscreenText(text = "Not Ready", pos = (0.2,0.4), 
        scale = 0.1,fg=(1,0.5,0.5,1),align=TextNode.ACenter,mayChange=1)
        
        self.b1 = DirectButton(text = ("Jouer", "!", "!", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command=self.go,
                          pos = (0,0,0.1))
        
           
        self.b2 = DirectButton(text = ("SiteWeb", "!", "!", "disabled"),
                          text_scale=0.09,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command=self.siteWeb,
                          pos = (0.95,0,-0.90))
  
        
    
    def username1Input(self,textEntered):
        #Quand on rentre notre username1 ca le save dans une variable (FAUT TOUJOURS PESER ENTER)
        self.username1 = textEntered
        
        
        
    def password1Input(self,textEntered):
        self.dao = DAOOracleUser()
        self.dto1 = DTOJoueur()
        dtoLog = DTOJoueur()
        
        self.password1 = textEntered
        
        if (self.username1 == None):
            self.username1 = self.entry1.get()
        
        #do test here!! user 1
        
        dtoLog = self.dao.readUserMDP(self.username1)
        
        
        

        try:
            hashedPass = dtoLog.getInfo('Password')
            self.dto1 = self.dao.userLogin(self.username1,hashedPass) 
            
            if(bcrypt.hashpw(self.password1, hashedPass) == hashedPass):
        
                self.initTank1()
                self.Ready1 =True
                self.textConfirm1.destroy()
                self.textConfirm1 =  textObject = OnscreenText(text = "Ready", pos = (-0.9,0.4), 
                                                           scale = 0.1,fg=self.green,align=TextNode.ACenter,mayChange=1)      
            else :
                 print('pas pareils')
                 self.textConfirm1.destroy()
                 self.textConfirm1 =  textObject = OnscreenText(text = "Info Invalide", pos = (-0.9,0.4), 
                                                           scale = 0.1,fg=self.red,align=TextNode.ACenter,mayChange=1) 
                 

             
        except : 
            print('oups ya un bug plus haut')
            self.textConfirm1.destroy()
            self.textConfirm1 =  textObject = OnscreenText(text = "Info Invalide", pos = (-0.9,0.4), 
                                                           scale = 0.1,fg=self.red,align=TextNode.ACenter,mayChange=1) 

    ##PLAYER 2
    def username2Input(self,textEntered):
        #rentre notre username2 et save dans une variable (FAUT TOUJOURS PESER ENTER)
        self.username2 = textEntered

    def password2Input(self,textEntered):
        
        self.dao = DAOOracleUser()
        self.dto2 = DTOJoueur()
        dtoLog = DTOJoueur()
        
        self.password2 = textEntered
        print(self.username2)
        print(self.password2)
        #do test here!! user 1
        if (self.username2 == None):
            self.username2 = self.entry2.get() 
            
        dtoLog = self.dao.readUserMDP(self.username2)
        try:
            hashedPass = dtoLog.getInfo('Password')
            if(bcrypt.hashpw(self.password2, hashedPass) == hashedPass):
                
                self.dto2 = self.dao.userLogin(self.username2,hashedPass)     
                self.initTank2()
                self.Ready2 =True
                self.textConfirm2.destroy()           
                self.textConfirm2 =  textObject = OnscreenText(text = "Ready", pos = (0.2,0.4), 
                                                           scale = 0.1,fg=self.green,align=TextNode.ACenter,mayChange=1) 
            else :
                 self.textConfirm2.destroy()
                 self.textConfirm2 =  textObject = OnscreenText(text = "Info Invalide", pos = (0.21,0.4), 
                                                           scale = 0.1,fg=self.red,align=TextNode.ACenter,mayChange=1)
                 
        except : 
            self.textConfirm2.destroy()
            self.textConfirm2 =  textObject = OnscreenText(text = "Info Invalide", pos = (0.21,0.4), 
                                                           scale = 0.1,fg=self.red,align=TextNode.ACenter,mayChange=1)
            
    def spinCameraTask(self, task):
        
        angleDegrees = task.time * 6.0
        angleRadians = angleDegrees * (pi / 180.0)
        camera.setPos(20 * sin(angleRadians), -20.0 * cos(angleRadians), 3)
        camera.setHpr(angleDegrees, 0, 0)
        return Task.cont
         
    def initTank1(self):
            
        #C'est ici qu;on get couleur (besoin du rgba) 
        couleur = self.dto1.getInfo('couleur').split(",")
        
        
        #on transforme en couleur accepter par panda3d et on rentre dans vector
        couleur = VBase4(int(couleur[0]) / 255.0, int(couleur[1]) / 255.0, int(couleur[2]) / 255.0,1)
        
        self.modele = loader.loadModel("../asset/Tank/tank")
        self.modele.setColorScale(couleur.getX(),couleur.getY(),couleur.getZ(),1)
           
        self.tank = Actor(self.modele, {'Animation Name 1':'Animation Path 1'})
        self.tank.setScale(-2, -2, 2)
        self.tank.reparentTo(render)
        self.tank.setPos(Point3(0, 7, 0))

        self.tankPosition = self.tank.posInterval(1.5,Point3(0,4,0),Point3(0, 7, 0))
        self.tankMove = Sequence(self.tankPosition)
        
    def initTank2(self):
        #C'est ici qu;on get couleur (besoin du rgba) 
        
        couleur2 = self.dto2.getInfo('couleur').split(",") #couleur rgba , dans ce cas (82, 239, 117)
        #on transforme en couleur accepter par panda3d et on rentre dans vector 
        couleur2 = VBase4(int(couleur2[0]) / 255.0, int(couleur2[1]) / 255.0, int(couleur2[2]) / 255.0,1)
        
        self.modele2 = loader.loadModel("../asset/Tank/tank")
        self.modele2.setColorScale(couleur2.getX(),couleur2.getY(),couleur2.getZ(),1)
        
        self.tank2 = Actor(self.modele2, {'Animation Name 1':'Animation Path 1'})
        self.tank2.setScale(2, 2, 2)
        self.tank2.reparentTo(render)
        self.tank2.setPos(Point3(0, -7, 0))

        self.tank2Position = self.tank2.posInterval(1.5, Point3(0, -4, 0), Point3(0, -7, 0),)
        self.tank2Move = Sequence(self.tank2Position)

    def siteWeb(self):
        #ouvre le site web (trop lazy pour linker) 
        Login()    
            
    def go (self):
        #lorsque les deux users sont pret on detruit pas mal tout et afficher le screen 2 
        if self.Ready1 is True and self.Ready2 is True:
            self.b2.destroy()
            self.b1.destroy()
            self.textObject1.destroy()
            self.entry1.destroy()
            self.textObjectPass1.destroy()
            self.entryPass1.destroy()
            self.textConfirm1.destroy()
            self.textObject2.destroy()
            self.entry2.destroy()
            self.textObjectPass2.destroy()
            self.entryPass2.destroy()
            self.textConfirm2.destroy()

        
            self.tankMove.start()
            self.tank2Move.start() 
            self.screen2()
            
    def screen2(self):
        
        self.textConfirm2 =OnscreenText(text = self.username1+" VS "+ self.username2, pos = (0,0.5), 
        scale = 0.25,fg=(1,0.5,0.5,1),align=TextNode.ACenter,mayChange=1)
        
        btnScale = (0.18,0.18)
        text_scale = 0.12
        borderW = (0.04, 0.04)
        couleurBack = (0.243,0.325,0.121,1)
        separation = 0.5
        hauteur = 0.5
        self.b1 = DirectButton(text = ("COMBATTRE", "!", "!", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command=self.launchGame,
                          pos = (0,0,0.1))
    
    

             
    def launchGame(self):
        tableauTank=[]
        tableauTank.append(self.dto1)
        tableauTank.append(self.dto2)
        self.scene.hide()
        self.textConfirm2.destroy()
        self.b1.destroy()
        taskMgr.remove("SpinCameraTask")
        self.tank.hide()
        self.tank2.hide()
        messenger.send("DemarrerPartie",tableauTank)     
        
   
        
        #print("jai passer ici")
        #self.gameLogic = menuVs(self)
        #tank= messenger.send("menuVS")""""

 
